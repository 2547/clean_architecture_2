import 'package:dartz/dartz.dart';
import 'package:intellij/core/errors/failures.dart';
import 'package:intellij/features/domain/entities/number_trivia.dart';

abstract class NumberTriviaRepository {
  Future<Either<Failure, NumberTrivia>> getConcreteNumberTrivia(int number);
  Future<Either<Failure, NumberTrivia>> getRandomNumberTrivia();
}