import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:intellij/core/errors/failures.dart';
import 'package:intellij/core/usecases/usecase.dart';
import 'package:intellij/features/domain/entities/number_trivia.dart';
import 'package:intellij/features/domain/repositories/number_trivia_repository.dart';

class GetConcreteNumberTrivia implements UseCase<NumberTrivia, Params> {

  final NumberTriviaRepository numberTriviaRepository;

  GetConcreteNumberTrivia(this.numberTriviaRepository);

  @override
  Future<Either<Failure, NumberTrivia>> call(Params params) async {
    return await numberTriviaRepository.getConcreteNumberTrivia(params.number);
  }
  
}

class Params extends Equatable {
  final int number;

  Params({@required this.number}) : super([number]);
}
