import 'package:dartz/dartz.dart';
import 'package:intellij/core/errors/failures.dart';
import 'package:intellij/core/usecases/usecase.dart';
import 'package:intellij/features/domain/entities/number_trivia.dart';
import 'package:intellij/features/domain/repositories/number_trivia_repository.dart';

class GetRandomNumberTrivia implements UseCase<NumberTrivia, NoParams> {

  final NumberTriviaRepository numberTriviaRepository;

  GetRandomNumberTrivia(this.numberTriviaRepository);

  @override
  Future<Either<Failure, NumberTrivia>> call(NoParams Params) async {
    return await this.numberTriviaRepository.getRandomNumberTrivia();
  }

}
